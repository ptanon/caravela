package ptchan_lame_api;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class PageProcessing {
	public static final String NO_TEXT = "";
	
	public static List<String> getAllBoards(String catalogHTML){
		ArrayList<String> boards = new ArrayList<>();
		
		String navHTML = catalogHTML.split("<div class=\"navbar\">")[1];
		navHTML = navHTML.split("</div>")[0];
		
		for(String line: navHTML.split("\n")){
			if (! line.contains("title"))
				continue;
			
			try{
				String board = line.split("href=\"/")[1].split("/")[0];
				boards.add(board);
			}
			catch(ArrayIndexOutOfBoundsException e){
				System.err.println("Error in following line: " + line);
				throw e;
			}

		}
		
		return boards;
	}

	public static List<String> getThreadsIDsFromCatalog(String board, String catalogHTML){
		ArrayList<String> threads = new ArrayList<>();

		String[] htmlLines = catalogHTML.split("\n");
		for(String line: htmlLines){
			String regex = "<a href=\"/" + board + "/res/";
			
			if (! line.contains(regex)){
				continue;
			}
			
			String regex1 = "/";
			String regex2 = ".h";
			String splitProcessing = line.split(regex1)[3];	
			splitProcessing = splitProcessing.split(regex2)[0];
			
			String threadID = splitProcessing;
			threads.add(threadID);
		}
		
		return threads;		
	}

	public static List<String> getPostsHtml(String board, String threadID, String threadHTML){
		ArrayList<String> posts = new ArrayList<>();

		String[] htmlLines = threadHTML.split("\n");
		int i = 0;
		for (; i < htmlLines.length; i++){
			String line = htmlLines[i];
			
			if (! line.contains("<table>"))
				continue;
			
			String currentPost = "";
			
			for (int j = i; j < htmlLines.length; j++){
				String postLine = htmlLines[j];
				
				if(postLine.contains("</table>")){
					posts.add(currentPost);
					// consider something like i=j here for nicer efficiency
					// but code that works comes before code that looks nice
					break;
				}
				else{
					currentPost += postLine + "\n";
				}
			}
		}
		
		return posts;
	}

	public static String getOPHtml(String board, String threadID, String threadHTML){
		String[] htmlLines = threadHTML.split("\n");
		
		for(int i = 0; i < htmlLines.length; i++){
			String line = htmlLines[i];
			
			if(! line.contains("<div id=\"thread"))
				continue;
			
			String currentPost = "";
			
			for(int j = i; i < htmlLines.length; j++){
				String postLine = htmlLines[j];
				
				if( postLine.contains("</div>"))
					return currentPost;
				
				else currentPost += postLine + "\n";
			}
		}
		return NO_TEXT;
	}

	public static List<String> getOPsHtmlFromBoardPage(String board, String boardPage, String boardPageHtml) {
		ArrayList<String> opsHtml = new ArrayList<>();
		String[] htmlLines = boardPageHtml.split("\n");

		for(int i = 0; i < htmlLines.length; i++){
			String line = htmlLines[i];

			if(! line.contains("<div id=\"thread")){
				continue;
			}

			String currentPostHtml = "";
			for(int j = i; j < htmlLines.length; j++){
				String postLine = htmlLines[j];

				if( postLine.contains("</div>")){
					opsHtml.add(currentPostHtml);
					i = j +1;
					break;
				}
				else{
					currentPostHtml += postLine + "\n";
				}
			}
		}
		return opsHtml;
	}
}
