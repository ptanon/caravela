package ptchan_lame_api;

import android.util.Log;

public class PostProcessing {
	public static final String NO_TEXT = "";
	public static final String NO_PIC = "No picture was posted";
	
	public static String getPostId(String postHTML){
		String[] htmlLines = postHTML.split("\n");
		
		for(String line : htmlLines){
			String regex = "<div id='post";
			
			if(! line.contains(regex))
				continue;
			
			String rgx1 = "<div id='post";
			String rgx2 = "'";

			String postID = line.split(rgx1)[1].split(rgx2)[0];
			return postID;
		}
		
		return NO_TEXT;
	}
	
	public static String getPostAuthor(String postHTML){
		
		for(String line: postHTML.split("\n")){
			
			if (! line.contains("<span class=\"postername\">"))
				continue;
			
			// sage, ex: <span class="postername"><a href="mailto:sage">Anónimo</a></span>
			String rgx1 = ">";
			String rgx2 = "<";
			
			String postAuthor;
			if ( line.contains("mailto")){
				postAuthor = line.split(rgx1)[2].split(rgx2)[0];
			}
			
			else{
				postAuthor = line.split(rgx1)[1].split(rgx2)[0];
			}
			
			return postAuthor;
		}
		
		return NO_TEXT;
	}
	
	public static String getEmail(String postHTML){
		
		for (String line: postHTML.split("\n")){
			
			if (! line.contains("mailto"))
				continue;
			
			String mailto = line.split("mailto:")[1].split("\"")[0];
			return mailto;
		}
		return NO_TEXT;
	}
	
	public static String getDate(String postHTML){
		String[] splitLines = postHTML.split("\n");
		
		for(int i = 0; i < splitLines.length; i++){
			String line = splitLines[i];
			String regex = "<span class=\"postername\">";
			
			if(! line.contains(regex))
				continue;
			
			String postDate = splitLines[i+2];
			return postDate;
		}
		
		return NO_TEXT;
	}

	public static String getAuthorID(String postHTML){
		String[] htmlLines = postHTML.split("\n");
		
		for(int i=1; i < htmlLines.length; i++){
			String previousLine = htmlLines[i-1];
			String currentLine = htmlLines[i];
			
			String rgx1 = "</span>";
			String rgx2 = "ID: ";
			
			if (! previousLine.contains(rgx1))
				continue;
			
			if (! currentLine.contains(rgx2))
				continue;
			
			String authorID = currentLine.replace("ID: ", "");
			return authorID;
		}
		return NO_TEXT;
	}

	public static String getPicURL(String postHTML){
		String[] splitLines = postHTML.split("\n");
		
		int i = 0;
		for(; i < splitLines.length; i++){
			String line = splitLines[i];
			String regex = "<span class=\"filesize\">";
			
			if (line.contains(regex))
				break;
		}
		
		if (i == splitLines.length)
			return NO_PIC;
		
		for(int j = i; j < splitLines.length; j++){
			String line = splitLines[j];
			
			String regex = "<a href=\"//www.ptchan.net/";
			
			if (! line.contains(regex))
				continue;
			
			String rgx1 = "<a href=\"//";
			String rgx2 = "\"";
			
			String picURL = line.split(rgx1)[1].split(rgx2)[0];
			return picURL;
		}
		return NO_PIC;
	}

	public static String getThumbURL(String picURL){
		if(picURL.equals(NO_PIC)){
			return NO_PIC;
		}

		String tempURL = picURL.replace("src", "thumb");

		String[] extensions = {".jpg", ".gif", ".png"};
		for(String extension: extensions){
			tempURL = tempURL.replace(extension, "s" + extension);
		}

		String thumbURL = tempURL;
		return thumbURL;
	}
	
	public static String getText(String postHTML){
		String[] splitLines = postHTML.split("\n");
		
		int i = 0;
		for(; i < splitLines.length; i++){
			String line = splitLines[i];
			
			if (line.contains("<div id='post"))
				break;
		}
		
		String content = "";
		
		for (int j=i; j < splitLines.length; j++){
			String line = splitLines[j];
			String endRgx = "</div>";
			
			if( line.contains(endRgx))
				return content;
			
			line = formatHtmlLine(line);
			content += line;
		}
		
		if(content.isEmpty())
			return NO_TEXT;
		else
			return content;
	}

	public static String formatHtmlLine(String line){
		// implying notation
		if (line .contains("<span class=\"unkfunc\">&gt;")){
			line = line.replace("<span class=\"unkfunc\">&gt;", ">");
			line = line.replace("\r</span>", "\n");
		}
		
		// remove div tag and id from post text
		if (line.contains("<div id='post")){
			String removeThis = line.split(">")[0] + ">";
			line = line.replace(removeThis, "");
		}

		// quotes, ex: <a href="/b/res/497164.html#497180"
		while (line.contains("return highlight")) {
			String quoted_post_id = line.split("#")[1].split("\"")[0];

			int quoteStartIndex = line.indexOf("<a");
			String beforeQuote = line.substring(0, quoteStartIndex);

			int quoteEndIndex = line.indexOf("</a>") + 4;
			String afterQuote = line.substring(quoteEndIndex);

			line = beforeQuote + ">>" + quoted_post_id + afterQuote;
		}

		// links, such as: <a href="http://tvmana1.com/">http://tvmana1.com/</a>
		while (line.contains("<a href")){

			int startOfTagIndex = line.indexOf("<a");
			String beforeTag = line.substring(0, startOfTagIndex);

			int endOfTagIndex = line.indexOf("</a>")+4;
			String afterTag = line.substring(endOfTagIndex);

			String tag = line.substring(startOfTagIndex, endOfTagIndex);
			String content = tag.split("\">")[1].split("</a")[0];

			line = beforeTag + content + afterTag;
		}

		line = replaceEscape(line);
		line = replaceOthers(line);
	
		return line;
	}

	public static String replaceEscape(String string){
		// characters used in html tags are escaped in plaint text
		string = string.replace("&quot;", "\"");
		string = string.replace("&#039;", "'");
		string = string.replace("&lt;", "<");
		string = string.replace("&gt;", ">");
		string = string.replace("&amp;", "&");

		// not sure about these, but they are escaped too
		string = string.replace("&#35;", "#");
		string = string.replace("&nbsp;", " ");

		return string;
	}

	public static String replaceOthers(String string){
		string = string.replace("<br />", "\n");
		string = string.replace("</span>", "");

		return string;
	}
}
