package ptchan_lame_api;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Network {
	public static final String PTCHAN_URL = "http://www.ptchan.net";
	public static final String PTCHAN_POST_URL = "http://www.ptchan.net/board.php";

	// Unfortunately we have to deviate a lot from the python implementation here due to different network libs
	public static String download(String url) throws IOException {
		URL myURL;
		try {
			myURL = new URL(url);
		} catch (MalformedURLException e) {
			return "";
		}

		String result = null;
		URLConnection connection;

		try {
			connection = myURL.openConnection();
			Scanner scanner = new Scanner(connection.getInputStream());

			// http://mtmurdockblog.com/2012/09/29/connecting-to-the-internet-via-urlconnection/
			// All streams start with the ‘\A’ (beginning of input boundary) character.
			// Using this as a delimiter with Scanner will result in a single String containing the full contents of the stream.
			result = scanner. useDelimiter("\\A").next();
			scanner.close();
		}

		catch (IOException e) {
			throw e;
		}

		return result;
	}

	// This lib is deprecated, but it's simple and cross platform. Android's recommended lib isn't as practical
	public static void post(String board, String threadID, String name, String message, String password){
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(PTCHAN_POST_URL);

		List<NameValuePair> fields = ptChanPostBuilder(board, threadID, name, message, password);

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(fields));
			HttpResponse response = httpClient.execute(httpPost);
		} catch (Exception e){
			Log.e(e.getMessage(), e.getStackTrace().toString());
		}
	}

	public static List<NameValuePair> ptChanPostBuilder(String board, String threadID, String name, String message,
												 String password){
		ArrayList<NameValuePair> postFields = new ArrayList<>();

		postFields.add(new BasicNameValuePair("board" , board));
		postFields.add(new BasicNameValuePair("replythread" , threadID));
		postFields.add(new BasicNameValuePair("MAX_FILE_SIZE", "3072000"));
		postFields.add(new BasicNameValuePair("id" , name));
		postFields.add(new BasicNameValuePair("message" , message));
		postFields.add(new BasicNameValuePair("postpassword" , password));

		return postFields;
	}

	public static String getBoardURL(String board, String page) {
		if (page.equals("0"))
			return PTCHAN_URL + "/" + board + "/";

		else return PTCHAN_URL + "/" + board + "/" + page + ".html";
	}

	public static String getCatalogURL(String board) {
		return PTCHAN_URL + "/" + board + "/" + "catalog.html";
	}

	public static String getThreadURL(String board, String threadID) {
		return PTCHAN_URL + "/" + board + "/res/" + threadID + ".html";
	}

	public static String downloadCatalog(String board) throws IOException {
		String bURL = getCatalogURL(board);
		return download(bURL);
	}

	public static String downloadBoard(String board, String page) throws IOException {
		String bURL = getBoardURL(board, page);
		return download(bURL);
	}

	public static String downloadThread(String board, String threadID) throws IOException {
		String tURL = getThreadURL(board, threadID);
		return download(tURL);
	}
}
