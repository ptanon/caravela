package com.example.imageboard;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.activities.R;

import java.util.ArrayList;

import ptchan_lame_api.PostProcessing;

import static ptchan_lame_api.PostProcessing.NO_PIC;
import static ptchan_lame_api.PostProcessing.getAuthorID;
import static ptchan_lame_api.PostProcessing.getDate;
import static ptchan_lame_api.PostProcessing.getEmail;
import static ptchan_lame_api.PostProcessing.getPostAuthor;
import static ptchan_lame_api.PostProcessing.getPostId;
import static ptchan_lame_api.PostProcessing.getText;
import static ptchan_lame_api.PostProcessing.getThumbURL;

public class Post {
    private static final float TEXT_SIZE = 15;
    private static final String HEADER_DELIMITER_DEFAULT = "   ";
    private static final String HEADER_DELIMITER_WHEN_THUMBNAIL_EXISTS = "\n";

    public final String board, threadID;
    private String postHTML;
    private String postID, postDate, postText, picURL, thumbURL, authorName, authorID, authorMail;

    private Image thumb = null;

    private LinearLayout postLayout;
    private LinearLayout headersAndImage;
    private TextView postMsg;

    public Post(String board, String threadID, String postHTML) {
        this.board = board;
        this.threadID = threadID;
        this.postHTML = postHTML;

        init();
    }

    private void init(){
        postID = getPostId(postHTML);
        postDate = getDate(postHTML);
        postText = getText(postHTML);
        picURL = PostProcessing.getPicURL(postHTML);
        thumbURL = getThumbURL(picURL);
        authorName = getPostAuthor(postHTML);
        authorID = getAuthorID(postHTML);
        authorMail = getEmail(postHTML);

        if(! picURL.equals(NO_PIC)){
            thumb = new Image(thumbURL);
        }
    }

    public void addToView(Context context, ViewGroup viewGroup){

        postLayout = new LinearLayout(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(5, 5, 5, 5);
        postLayout.setLayoutParams(params);
        postLayout.setPadding(10, 10, 10, 10);
        postLayout.setOrientation(LinearLayout.VERTICAL);

        headersAndImage = new LinearLayout(context);
        headersAndImage.setOrientation(LinearLayout.HORIZONTAL);
        headersAndImage.setPadding(10, 0, 0, 0);
        postLayout.addView(headersAndImage);

        String headerDelimiter = HEADER_DELIMITER_DEFAULT;
        if(thumb != null){
            thumb.addToView(context, headersAndImage);
            thumb.getImgProxy().setPadding(0,0,0,10);
            headerDelimiter = HEADER_DELIMITER_WHEN_THUMBNAIL_EXISTS;
        }


        View headerLayout = generateHeaderLayout(context, headerDelimiter);
        headerLayout.setPadding(0, 0, 0, 10);

        headersAndImage.addView(headerLayout);

        postMsg = generatePostMsgView(context);
        postLayout.addView(postMsg);

        postLayout.setBackgroundColor(Color.GRAY);
        viewGroup.addView(postLayout);
    }

    private View generateHeaderLayout(Context context, String headerDelimiter) {
        ArrayList<String> headersText = new ArrayList<>();

        headersText.add(this.authorName) ;
        headersText.add(this.postDate);
        headersText.add(context.getString(R.string.POST_ID) + " " + this.postID);
        headersText.add(context.getString(R.string.AUTHOR_ID) + ": " + authorID);

        if (this.authorMail != null && !this.authorMail.isEmpty()){
            headersText.add(context.getString(R.string.EMAIL) + ":" + this.authorMail);
        }

        String textMsg = "";
        for(String header: headersText){
            textMsg+= header + headerDelimiter;
        }

        TextView headersView = new TextView(context);
        headersView.setText(textMsg.trim());

        headersView.setTextColor(Color.rgb(20, 20, 20));
        return headersView;
    }

    private TextView generatePostMsgView(Context context){
        String content = postText;

        TextView postView = new TextView(context);
        postView.setText(content);
        postView.setTextSize(TEXT_SIZE);
        postView.setTextColor(Color.rgb(20, 20, 20));

        return postView;
    }

    public void setOnClickListener(View.OnClickListener l){
        postLayout.setOnClickListener(l);
    }

    public void addThumbOnClickListener(View.OnClickListener l){
        if(thumb != null){
            thumb.setOnClickListener(l);
        }
    }

    public String getPostID() {
        return postID;
    }
    public String getPicURL() {
        return picURL;
    }
    public LinearLayout getPostLayout() {
        return postLayout;
    }
    public boolean hasImage() {
        return ! picURL.equals(NO_PIC);
    }
}
