package com.example.imageboard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ptchan_lame_api.Network;
import ptchan_lame_api.PageProcessing;

public class BoardsList {
    private static final String catalogToFetchBoardsFrom = "b";
    private ArrayList<String> boardsList = new ArrayList<String>();

    public BoardsList() throws IOException {
        String html = Network.downloadCatalog(catalogToFetchBoardsFrom);

        for (String board: PageProcessing.getAllBoards(html)){
            boardsList.add(board);
        }
    }

    public List<String> getAllBoards(){
        return this.boardsList;
    }


}
