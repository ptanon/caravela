package com.example.imageboard;


import java.io.IOException;
import java.util.ArrayList;

import ptchan_lame_api.Network;
import ptchan_lame_api.PageProcessing;

public class Catalog {
    // board id must be abc, not /abc/
    public final String boardName;
    public final String catalogHTML;

    private ArrayList<String> threadsIDList = new ArrayList<String>();

    public Catalog(String boardName) throws IOException {
        this.boardName = boardName;
        this.catalogHTML = Network.downloadCatalog(boardName);

        init();
    }

    private void init(){
        for(String threadID : PageProcessing.getThreadsIDsFromCatalog(boardName, catalogHTML)){
            threadsIDList.add(threadID);
        }
    }

    public ArrayList<String> getThreadsIDList() {
        return threadsIDList;
    }
}
