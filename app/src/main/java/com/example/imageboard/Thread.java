package com.example.imageboard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ptchan_lame_api.Network;
import ptchan_lame_api.PageProcessing;

public class Thread {
    public final String board;
    public final String threadID;
    private String threadHTML;

    private Post op;
    private ArrayList<Post> posts = new ArrayList<Post>();

    public Thread(String board, String threadID, String threadHTML){
        this.board = board;
        this.threadID = threadID;
        this.threadHTML = threadHTML;

        init();
    }

    public Thread(String board, String threadID) throws IOException {
        this.board = board;
        this.threadID = threadID;
        this.threadHTML = Network.downloadThread(board, threadID);

        init();
    }

    private void init() {
        String opHTML = PageProcessing.getOPHtml(board, threadID, threadHTML);
        op = new Post(board, threadID, opHTML);

        List<String> postsHTML = PageProcessing.getPostsHtml(board, threadID, threadHTML);

        for (String postHTML : postsHTML) {
            Post p = new Post(board, threadID, postHTML);
            posts.add(p);
        }
    }

    public Post getOP(){
        return this.op;
    }

    public List<Post> getPosts(){
        return this.posts;
    }

    public List<Post> getAllPosts(){
        ArrayList<Post> allPosts = new ArrayList();
        allPosts.add(op);
        allPosts.addAll(posts);

        return allPosts;
    }
}
