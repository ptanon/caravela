package com.example.imageboard;

import java.io.IOException;
import java.util.ArrayList;

import ptchan_lame_api.Network;
import ptchan_lame_api.PageProcessing;
import ptchan_lame_api.PostProcessing;

public class BoardPage {
    public final String board;
    public final String page;

    private ArrayList<Post> listOfOPs;

    public BoardPage(String board, String page) throws IOException {
        this.board = board;
        this.page = page;

        init();
    }

    private void init() throws IOException {
        listOfOPs = new ArrayList<>();
        String boardPageHTML = Network.downloadBoard(board, page);

        for(String postHTML: PageProcessing.getOPsHtmlFromBoardPage(board, page, boardPageHTML)){
            String threadID = PostProcessing.getPostId(postHTML);
            Post op = new Post(board, threadID, postHTML);
            listOfOPs.add(op);
        }
    }

    public void refresh() throws IOException {
        init();
    }

    public ArrayList<Post> getListOfOPs() {
        return listOfOPs;
    }

    public String getBoard() {
        return board;
    }

    public String getPage() {
        return page;
    }
}
