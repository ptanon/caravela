package com.example.imageboard;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.example.activities.MainActivity;
import com.example.activities.R;
import com.example.utilities.AsyncImgDownload;

import static ptchan_lame_api.PostProcessing.NO_PIC;

public class Image {
    public final String url;
    public String board;
    public String id;
    public String fileType;

    private AsyncImgDownload imgFetcher;

    private ViewSwitcher imgProxy;
    private ViewGroup viewGroup;
    private Context context;


    public Image(String url){
        this.url = url;

        loadDetails(url);
    }

    private void loadDetails(String url){
        if (url.isEmpty()){
            return;
        }

        String[] urlSplit = url.split("/");
        this.board = urlSplit[urlSplit.length-3];
        this.id = urlSplit[urlSplit.length-1].split("\\.")[0];
        this.fileType = urlSplit[urlSplit.length-1].split("\\.")[1];
    }


    public void addToView(Context context, ViewGroup viewGroup) {
        this.viewGroup = viewGroup;
        this.context = context;

        addProxy();
        fetchThumbToView();
    }

    public void addProxy() {
        imgProxy = new ViewSwitcher(context);
        TextView proxyText = new TextView(context);
        proxyText.setText(R.string.IMG_LOADING);

        imgProxy.addView(proxyText);
        viewGroup.addView(imgProxy);
    }


    private void fetchThumbToView() {
        if (url.equals(NO_PIC)) {
            Toast toast = Toast.makeText(context, "Image was created with a bad URL - no download attempted.", Toast.LENGTH_LONG);
            toast.show();
            return;
        }

        imgFetcher = new AsyncImgDownload(context, this);
        imgFetcher.execute();
    }

    // called by AsyncImgDownload
    public void inject(Drawable drawable){
        if (drawable == null){
            Toast toast = Toast.makeText(context, imgFetcher.getStatusMessage(), Toast.LENGTH_LONG);
            toast.show();
            return;
        }
        setImage(drawable);
    }

    public void setImage(Drawable drawable){
        float scaleFactor = MainActivity.SCALE_FACTOR;
        int height = (int) (drawable.getIntrinsicHeight() * scaleFactor);
        int width = (int) (drawable.getIntrinsicWidth() * scaleFactor);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
        imgProxy.setLayoutParams(params);

        ImageView thumbView = new ImageView(context);
        thumbView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        thumbView.setImageDrawable(drawable);

        addThumb(thumbView);
    }

    public void addThumb(ImageView thumbView){
        imgProxy.addView(thumbView);
        imgProxy.showNext();
    }

    public void setOnClickListener(View.OnClickListener l) {
        imgProxy.setOnClickListener(l);
    }

    public ViewSwitcher getImgProxy() {
        return imgProxy;
    }
}
