package com.example.utilities;

import android.os.AsyncTask;

import java.io.IOException;

import ptchan_lame_api.Network;

/** Wraps the Network class from PTchan Lame API to fetch html in a background task.
 */
public class AsyncHtmlDownload {
    private String result = "";

    private class DownloadTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... urls) {
            String[] htmlPages = new String[urls.length];

            try {
                for (int i = 0; i < urls.length; i++) {
                    htmlPages[i] = Network.download(urls[i]);
                }
            }
            catch(IOException e){
                //FIXME: use status codes
                return null;
            }

            return htmlPages[0];
        }
    }



    /*
    TODO: use proxies and multithreading for html fetching
    public String downloadCatalog(String board) {
        String cURL = Network.getCatalogURL(board);
        String html = new DownloadTask().execute(cURL);

        return html;
    }

    public String downloadBoard(String board, String page) {
        String bURL = Network.getBoardURL(board, page);
        DownloadTask dt = new DownloadTask();
        String html = dt.execute(bURL);

        return html;
    }

    public String downloadThread(String board, String threadID) {
        String tURL = Network.getThreadURL(board, threadID);
        String html = new DownloadTask().execute(tURL);

        return html;
    }
    */
}
