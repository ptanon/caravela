package com.example.utilities;

import android.content.Context;
import android.util.Log;

import com.example.imageboard.Image;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created on 8/19/15.
 */
public class ImageCache {
    private Image image;
    private Context context;

    public ImageCache(Context context, Image image) {
        this.context = context;
        this.image = image;
    }

    public void save(InputStream stream){
        String url = image.url;
        String board = image.board;
        String id = image.id;
        String fileType = image.fileType;

        String type;
        if(url.contains("thumb")){
            type = "thumb";
        } else{
            type = "src";
        }

        File dir = context.getDir(type, Context.MODE_PRIVATE);
        Log.e("DIR", dir.getAbsolutePath().toString());

        String name = type + "-" + board + "-" + id + "." + fileType;
        File img = new File(dir.getAbsolutePath(), name);

        try {
            FileOutputStream fos = context.openFileOutput(name, Context.MODE_PRIVATE);
            fos.write(stream.read());
            fos.close();
        }
        catch(FileNotFoundException e){
            Log.e("File not found", name);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
