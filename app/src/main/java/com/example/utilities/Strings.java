package com.example.utilities;

/**
 * Created on 8/20/15.
 */
public class Strings {
    /**
     * Believe it or not, forward slashes are OS dependent - a pain in the ass.
     *
     * @param url A PTchan url
     * @return A url slash
     */
    public static String getSlash(String url){
        String[] tmpSplit = url.split(".net");

        return "" + tmpSplit[1].charAt(0);
    }
}
