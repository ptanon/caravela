package com.example.utilities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import com.example.imageboard.Image;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


public class AsyncImgDownload extends AsyncTask<Void, Void, Void> implements Runnable{
    public static final int NOT_INITIALIZED = 0;
    public static final int INITIALIZED = 1;
    public static final int DOWNLOADING = 2;
    public static final int DOWNLOAD_SUCCESS = 3;
    public static final int DOWNLOAD_FAIL = 4;

    private final Image image;
    private final String url;
    private Drawable pic = null;
    private ImageCache imgCache;
    private int statusCode = NOT_INITIALIZED;

    public AsyncImgDownload(Context context, Image image){
        this.image = image;
        this.url = image.url;
        statusCode = INITIALIZED;

        imgCache = new ImageCache(context, image);
    }

    @Override
    public void run(){
        // CACHE - check

        InputStream stream;
        try {
            stream = download();
            //imgCache.save(stream);
        }
        catch (IOException e) {
            Log.e("IOException", e.getMessage());
            this.statusCode = DOWNLOAD_FAIL;
            return;
        }

        pic = Drawable.createFromStream(stream, "src");
        statusCode = DOWNLOAD_SUCCESS;
    }

    private InputStream download() throws IOException {
        statusCode = DOWNLOADING;
        URL url = new URL("http://" + this.url);
       return url.openStream();

    }

    @Override
    protected Void doInBackground(Void... params) {
        run();
        return null;
    }

    @Override
    protected void onPostExecute(Void param){
        notifyImage();
    }

    public void notifyImage(){
        image.inject(pic);
    }

    public String getStatusMessage(){

        switch(statusCode) {
            case (NOT_INITIALIZED):
                return "Image fetcher was not initialized properly.";
            case (INITIALIZED):
                return "Image fetcher was initialized but wasn't asked to do anything yet";
            case(DOWNLOADING):
                return "Image fetcher is attempting download now from" + this.url;
            case(DOWNLOAD_SUCCESS):
                return "Image fetcher successfully downloaded from " + this.url;
            case(DOWNLOAD_FAIL):
                return "Image fetcher failed to download from " + this.url;

            default:
                return "Unknown status code on Image Fetcher. URL for reference: " + this.url;
        }
    }
}
