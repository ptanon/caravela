package com.example.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.imageboard.Post;
import com.example.imageboard.Thread;

import java.io.IOException;
import java.util.List;


public class ThreadNavigation extends AppCompatActivity {
    String threadID;
    String board;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        board = intent.getStringExtra(MainActivity.BOARD_SELECTED);
        threadID = intent.getStringExtra(CatalogNavigation.THREAD_SELECTED);
        String title = "/" + board + "/ - " + threadID;
        setTitle(title);

        setContentView(R.layout.activity_thread_navigation);

        Thread thread;
        try {
            thread = new Thread(board, threadID);
        }
        catch (IOException e) {
            Toast toast = Toast.makeText(getApplicationContext(), "Error downloading - " +
                    "is your device connected to the Internet?", Toast.LENGTH_LONG);
            toast.show();
            return;
        }
        showAllPosts(thread);

        Button button = (Button) findViewById(R.id.replyButton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                replyToThread();
            }
        });
    }

    public void replyToThread(){
        Intent intent = new Intent(this, PostActivity.class);
        intent.putExtra(PostActivity.BOARD_INTENT, this.board);
        intent.putExtra(PostActivity.THREAD_ID_INTENT, this.threadID);

        startActivity(intent);
    }

    public void showAllPosts(Thread thread) {

        Context context = getApplicationContext();
        ViewGroup layout = (ViewGroup) findViewById(R.id.threadnavigation_linearlayout);

        List<Post> posts = thread.getAllPosts();
        for (int i = 0; i < posts.size(); i++) {
            final Post post = posts.get(i);

            int bgValue = ((i % 2) * 15) + 210;
            int bgColor = Color.rgb(bgValue, bgValue, bgValue);

            post.addToView(context, layout);
            post.getPostLayout().setBackgroundColor(bgColor);

            if(post.hasImage()){
                post.addThumbOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        goToImage(post.getPicURL());
                    }
                });
            }
        }
    }

    public void goToImage(String picURL){
        Intent intent = new Intent(getApplicationContext(), ImageActivity.class);
        intent.putExtra(ImageActivity.IMG_URL_INTENT, picURL);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_thread_navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
