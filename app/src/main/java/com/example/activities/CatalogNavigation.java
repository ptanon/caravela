package com.example.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.imageboard.Catalog;

import java.io.IOException;
import java.util.List;


public class CatalogNavigation extends ActionBarActivity {

    public static String THREAD_SELECTED = "Thread selected by user";
    private String board = "";
    private String title = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        board = intent.getStringExtra(MainActivity.BOARD_SELECTED);
        title = "/" + board + "/";
        setTitle(title);
        setContentView(R.layout.activity_catalog_navigation);


        Catalog catalog = null;
        try {
            catalog = new Catalog(board);
        } catch (IOException e) {
            Toast toast = Toast.makeText(getApplicationContext(), "Error downloading - " +
                    "is your device connected to the Internet?", Toast.LENGTH_LONG);
            toast.show();
            return;
        }
        showWarning();
        showThreads(catalog);
    }

    protected void showThreads(Catalog catalog){
        List<String> threadsIDList = catalog.getThreadsIDList();

        try{
            for(String threadID: threadsIDList) {
                addThreadButton(board, threadID);
            }
        }
        catch(Exception e) {
            //fuck it
        }
    }

    public void addThreadButton(final String board, final String threadID){

        Button button = new Button(getApplicationContext());
        button.setText(threadID);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goToThread(board, threadID);
            }
        });

        button.setLayoutParams(new ViewGroup.LayoutParams
                (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        // TODO: confirm if gravity goes here or on parent layout
        button.setGravity(Gravity.CENTER);

        ViewGroup layout = (ViewGroup) findViewById(R.id.boardnavigation_linearlayout);
        layout.addView(button);
    }

    public void goToThread(String board, String threadID){
        Intent intent = new Intent(this, ThreadNavigation.class);
        intent.putExtra(MainActivity.BOARD_SELECTED, board);
        intent.putExtra(THREAD_SELECTED, threadID);
        startActivity(intent);
    }

    public void showWarning(){
        String warning = "Pré-visualização de threads será implementada mais tarde.";

        TextView textView = new TextView(getApplicationContext());
        textView.setText(warning);
        textView.setBackgroundColor(Color.YELLOW);
        textView.setTextColor(Color.BLACK);
        ViewGroup layout = (ViewGroup) findViewById(R.id.boardnavigation_linearlayout);
        layout.addView(textView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_board_navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
