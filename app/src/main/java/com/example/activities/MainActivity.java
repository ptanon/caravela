package com.example.activities;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.example.androidpatterns.BoardsAdapter;
import com.example.imageboard.BoardsList;

import java.io.IOException;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    public static float SCALE_FACTOR = 0f;
    public static final String BOARD_SELECTED = "Catalog selected by user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // FIXME: this should be dynamic, as the screen can be rotated
        SCALE_FACTOR = calculateScaleFactor();

        initBoards();
    }

    public void initBoards(){
        BoardsList boardsList;
        try {
            boardsList = new BoardsList();
        } catch (IOException e) {
            Toast toast = Toast.makeText(getApplicationContext(), "Error downloading - " +
                    "is your device connected to the Internet?", Toast.LENGTH_LONG);
            toast.show();
            return;
        }

        List<String> boards = boardsList.getAllBoards();
        //boards.add("tes");
        BoardsAdapter boardsAdapter = new BoardsAdapter(getApplicationContext(), boards);

        for(int i = 0; i < boardsAdapter.getAllButtons().size(); i++){
            Button b = boardsAdapter.getAllButtons().get(i);
            final String board = boardsAdapter.getAllBoards().get(i);

            b.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    goToBoard(board);
                }
            });
        }

        int nColumns = 3;

        GridView rootLayout = (GridView) findViewById(R.id.boardselection_linearlayout);
        rootLayout.setNumColumns(nColumns);
        rootLayout.setGravity(Gravity.CENTER);
        rootLayout.setAdapter(boardsAdapter);
    }

    public void refresh(){
        initBoards();
    }

    private float calculateScaleFactor() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = size.x;
        //int height = size.y;

        float scale;
        if(width > 1000){
            scale = 2f;
        }
        else if (width > 800){
            scale = 1.75f;
        }
        else if (width > 600){
            scale = 1.5f;
        }
        else{
            scale = 1.25f;
        }

        return scale;
    }

    public void goToBoard(String board){
        Intent intent = new Intent(this, BoardPageNavigation.class);
        intent.putExtra(BOARD_SELECTED, board);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
