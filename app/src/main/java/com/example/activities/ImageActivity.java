package com.example.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.example.imageboard.Image;

public class ImageActivity extends AppCompatActivity {
    public static final String IMG_URL_INTENT = "URL to download image.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);



        View view = findViewById(R.id.parent_view);
        view.setBackgroundColor(Color.BLACK);

        Intent intent = getIntent();
        if(! intent.hasExtra(IMG_URL_INTENT)){
            this.finish();
        }

        String url = intent.getStringExtra(IMG_URL_INTENT);
        setTitle(url);
        LinearLayout layout = (LinearLayout) findViewById(R.id.activity_image);
        Image img = new Image(url);
        img.addToView(getApplicationContext(), layout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
