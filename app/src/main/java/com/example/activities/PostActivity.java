package com.example.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ptchan_lame_api.Network;

public class PostActivity extends AppCompatActivity {
    public static final String BOARD_INTENT = "BOARD_INTENT";
    public static final String THREAD_ID_INTENT = "THREAD_ID_INTENT";

    public static String board, threadID;
    public static String name, password, postMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        board = getIntent().getStringExtra(BOARD_INTENT);
        threadID = getIntent().getStringExtra(THREAD_ID_INTENT);

        Button button = (Button) findViewById(R.id.submitButton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                submitPost();
            }
        });
    }

    public void submitPost(){
        readTextFields();

        Log.i("board", board);
        Log.i("threadID", threadID);
        Log.i("id", name);
        Log.i("postMessage", postMessage);
        Log.i("password", password);

        Network.post(board, threadID, name, postMessage, password);
        this.finish();
    }

    public void readTextFields(){
        this.name = ((EditText) findViewById(R.id.editTextName)).getText().toString();
        this.password = ((EditText) findViewById(R.id.editTextPassword)).getText().toString();
        this.postMessage = ((EditText) findViewById(R.id.editTextMessage)).getText().toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
