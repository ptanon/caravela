package com.example.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.imageboard.BoardPage;
import com.example.imageboard.Post;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class BoardPageNavigation extends AppCompatActivity {
    public static String THREAD_SELECTED = "Thread selected by user";
    private String board = "";

    private ArrayList<BoardPage> boardPages = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        board = intent.getStringExtra(MainActivity.BOARD_SELECTED);
        setTitle("/" + board + "/");
        setContentView(R.layout.activity_board_page_navigation);

        initBoardPages();

    }

    public void initBoardPages(){
        String page = "0";
        BoardPage boardPage;
        try {
            boardPage = new BoardPage(board, page);
        } catch (IOException e) {
            Toast toast = Toast.makeText(getApplicationContext(), "Error downloading - " +
                    "is your device connected to the Internet?", Toast.LENGTH_LONG);
            toast.show();
            return;
        }
        boardPages.add(boardPage);

        ViewGroup parentLayout = (ViewGroup) findViewById(R.id.boardpagenavigation_linearlayout);
        showWarning();
        showOPs(boardPage, parentLayout);
        addNextPageButton(parentLayout);
    }

    public void showOPs(BoardPage boardPage, ViewGroup parentLayout){;

        TextView textView = new TextView(getApplicationContext());
        textView.setText(getString(R.string.PAGE) + ": " + boardPage.getPage());
        parentLayout.addView(textView);

        List<Post> opList = boardPage.getListOfOPs();
        for(int i = 0; i < opList.size(); i++){
            final Post post = opList.get(i);
            Context context = getApplicationContext();
            post.addToView(context, parentLayout);

            post.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    goToThread(board, post.getPostID());
                }
            });

            post.addThumbOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    goToImage(post.getPicURL());
                }
            });

            int bgValue = ((i % 2) * 15) + 210;
            int bgColor = Color.rgb(bgValue, bgValue, bgValue);
            post.getPostLayout().setBackgroundColor(bgColor);
        }
    }

    public void addNextPageButton(ViewGroup parentLayout){
        Button button = new Button(getApplicationContext());
        button.setText("Next page: ");
        parentLayout.addView(button);

        final Button b = button;

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addNextPage(b);
            }
        });
    }

    public void goToImage(String picURL){
        Intent intent = new Intent(getApplicationContext(), ImageActivity.class);
        intent.putExtra(ImageActivity.IMG_URL_INTENT, picURL);
        startActivity(intent);
    }

    public void addNextPage(Button b){
        String nextPageIndex = "" + boardPages.size();
        ViewGroup parentLayout = (ViewGroup) findViewById(R.id.boardpagenavigation_linearlayout);

        try {
            BoardPage nextPage = new BoardPage(board, nextPageIndex);
            showOPs(nextPage, parentLayout);
            boardPages.add(nextPage);
            b.setVisibility(View.GONE);
        }
        catch(Exception e){
            Log.e("fuck", Log.getStackTraceString(e));
        }
    }

    public void refresh(){
        ViewGroup parentLayout = (ViewGroup) findViewById(R.id.boardpagenavigation_linearlayout);
        parentLayout.removeAllViewsInLayout();

        if(boardPages.isEmpty()){
            initBoardPages();
            return;
        }

        for(BoardPage bp: boardPages){
            try{
                bp.refresh();
                showOPs(bp, parentLayout);
            }
            catch(IOException e){
                Toast toast = Toast.makeText(getApplicationContext(),
                    "Error downloading [board page: " + bp.getPage() + " - is your device connected to the Internet?",
                    Toast.LENGTH_LONG);
                toast.show();
            }
        }

        if(! boardPages.isEmpty()){
            addNextPageButton(parentLayout);
        }
    }

    public void goToThread(String board, String threadID){
        Intent intent = new Intent(this, ThreadNavigation.class);
        intent.putExtra(MainActivity.BOARD_SELECTED, board);
        intent.putExtra(THREAD_SELECTED, threadID);
        startActivity(intent);
    }

    public void showWarning(){
        String warning = "Tocar na imagem de um OP para expandir. Tocar num OP para abrir thread.";

        TextView textView = new TextView(getApplicationContext());
        textView.setText(warning);
        textView.setBackgroundColor(Color.YELLOW);
        textView.setTextColor(Color.BLACK);
        ViewGroup layout = (ViewGroup) findViewById(R.id.boardpagenavigation_linearlayout);
        layout.addView(textView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_board_page_navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
