package com.example.androidpatterns;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class BoardsAdapter extends BaseAdapter {
    ArrayList<String> boards;
    ArrayList<Button> buttons = new ArrayList<>();
    Context context;

    public BoardsAdapter(Context context, List<String> boards) {
        this.boards = new ArrayList<>(boards);
        this.context = context;


        for (String board : boards) {
            Button button = new Button(this.context);
            String buttonText = "/" + board + "/";
            button.setText(buttonText);
            button.setTextColor(Color.rgb(20,20,20));
            button.setMinimumHeight(100);
            button.setMinimumWidth(100);

            buttons.add(button);
        }

        int nButtons = buttons.size();
        for (int i = 0; i < nButtons; i++) {
            Button button = buttons.get(i);

            int bgValue = i%2 * 30 + 180;
            int bgColor = Color.rgb(bgValue, bgValue, bgValue);
            button.setBackgroundColor(bgColor);
        }
    }

    @Override
    public int getCount() {
        return boards.size();
    }

    @Override
    public Object getItem(int position) {
        return buttons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return buttons.get(position);
    }

    public List<Button> getAllButtons(){
        return buttons;
    }

    public List<String> getAllBoards(){
        return boards;
    }
}
